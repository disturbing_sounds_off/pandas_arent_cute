import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)


my_data = pd.read_csv("index.txt", sep="\s+", usecols=['Lat','Mort']) 
my_data.info()
print("\n")
print("----------------------------------------")
print("\n")
print(my_data.head())
my_data.head()

ax = sns.scatterplot(x="Lat", y="Mort", data=my_data) 

lat = my_data['Lat'].to_numpy().reshape(len(my_data["Lat"]), 1)
mort = my_data['Mort'].to_numpy().reshape(len(my_data["Mort"]), 1)

regression_model = LinearRegression() # создаем модель - линейная регрессия
regression_model.fit(lat, mort)     # обучаем модель
h = regression_model.predict(lat)    # предсказываем значения

plt.title("Предсказатеь твоей СМЕРТИ") 
# plt.scatter(lat, mort)
plt.xlabel("широта")
plt.ylabel("СМЕРТИ")
plt.plot(lat, h, color='r')

latitude = float(input("Введите широту: "))
prediction = regression_model.predict([[latitude]])
print("На этой широте: %.2f  \nУмрет %.2f людей" % (latitude, prediction))

plt.show()

