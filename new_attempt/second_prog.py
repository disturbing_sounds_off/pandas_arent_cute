import pandas as pd
from sklearn.linear_model import LinearRegression
import requests
import re
# import warnings
# warnings.filterwarnings("ignore", category=DeprecationWarning)

def get_geo_object(query_string):
  GEO_URL = 'https://geocode.maps.co/search?'
  query_string = re.sub('[\s,]+', '+', query_string)  # замена в строке поиска символы запятой и пробела на символ плюс
  search_string = 'q=' + query_string
  response = requests.get(GEO_URL, params=search_string)
  return response.url, response.json()


my_data = pd.read_csv("index.txt", sep="\s+", usecols=['Lat','Mort']) 

lat = my_data['Lat'].to_numpy().reshape(len(my_data["Lat"]), 1)
mort = my_data['Mort'].to_numpy().reshape(len(my_data["Mort"]), 1)

regression_model = LinearRegression() 
regression_model.fit(lat, mort)

query = (input("Введите адрес: "))
# query = "1001 Van Ness Ave, San Francisco, CA 94109"
url, response = get_geo_object(query)
latitude = float(response[0]["lat"])

prediction = regression_model.predict([[latitude]])
print("На этой широте: %.2f" % (latitude))
print("Умрет людей:", int(prediction))


