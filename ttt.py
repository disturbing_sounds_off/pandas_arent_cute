import seaborn as sns # импортируем библиотеку построения графиков
import matplotlib.pyplot as plt # импортируем библиотеку содержащую функцию построения прямой линии
import pandas as pd  # импорт библиотеки pandas для манипуляции данными
import numpy as np

houses_df = pd.read_csv('house-prices.csv') # открываем файл для анализа
houses_df.head()  # оцениваем содержимое таблицы
houses_df.info()

ax = sns.scatterplot(x="SqFt", y="Price", data=houses_df) # строим график
plt.plot([1400, 2600], [60000, 200000], linewidth=2) # добавляем на график прямую линию


x = np.linspace(0, 2, 100)
y = 2.35*x**2 - 4.68 * x + 2.33

fig = plt.figure()
fig.set_size_inches(8, 8)
ax = fig.add_subplot(1, 1, 1)
ax.set_xlim([0, 2])
ax.set_ylim([0, 4])
ax.set_xlabel(r'$ϴ_1$')
ax.set_ylabel(r'$J(ϴ_1)$')
ax.set_title(r'$J(ϴ_1)$')
ax.scatter([1, 0.5, 0], [0, 0.58, 2.33], color = 'blue')
ax.plot(x, y)

fig = plt.figure()
fig.set_size_inches(8, 8)
ax1 = fig.add_subplot(1, 1, 1)
ax1.set_xlim([0, 9])
ax1.set_ylim([0, 9])
ax1.set_xlabel('x')
ax1.set_ylabel('f(x)')
ax1.scatter([1,3,5,7], [1,4,6,4], color = 'red', marker = 'x')
ax1.plot([0,2,4,6,8], [0,2,6,6,2])
ax1.grid(linestyle = '--')

def parabola(x):
	return x**2

def derivative(x):
  return x*2

x = np.linspace(-10, 10, 100)
y = parabola(x)

solutions = []
evaluations = []

# learning_rate = 0.1
# learning_rate = 0.9
learning_rate = 1.2

# Q = 9
Q = -8

for i in range(20):
  solutions.append(Q)
  evaluations.append(parabola(Q))

  print('%d: f(%.5f) = %.5f, derivative = %.5f' % (i, Q, parabola(Q), derivative(Q)))
  Q = Q - learning_rate * derivative(Q)


plt.plot(x, y)
plt.scatter(solutions, evaluations, color='red')

print("houses_df['SqFt']")
print(houses_df['SqFt'][:10], '\n')
print("houses_df['SqFt'].to_numpy()")
print(houses_df['SqFt'].to_numpy()[:10], '\n')
print("houses_df['SqFt'].to_numpy().reshape(len(houses_df['SqFt']), 1)")
print(houses_df['SqFt'].to_numpy().reshape(len(houses_df['SqFt']), 1)[:10])

plt.show()
