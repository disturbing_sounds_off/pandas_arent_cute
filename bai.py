
import matplotlib.pyplot as plt

# Заданные значения точек
x = [0, 1, 2, 3, 4, 5]
y = [17.3, 17.3, 9, 3.5, 1.1, 0.5]

# Настройки графика
plt.plot(x, y, marker='o')

# Добавляем вспомогательные линии
for i in range(len(x)):
    plt.vlines(x[i], ymin=0, ymax=y[i], linestyles='dotted', colors='gray')

# Ограничение по осям y
plt.ylim(0, 20)

# Отображение графика
plt.show()
