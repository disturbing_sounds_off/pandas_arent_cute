from keras_tuner.tuners import RandomSearch, Hyperband, BayesianOptimization
import numpy as np
from keras.datasets import cifar10
from keras.models import Sequential
from keras.layers import Dense, Flatten
from keras.layers import Dropout
from keras.layers import Conv2D, MaxPooling2D
from keras.utils import to_categorical
from keras.preprocessing import image
import matplotlib.pyplot as plt

batch_size = 128
nb_classes = 10
nb_epoch = 25
img_rows, img_cols = 32, 32
img_channels = 3
classes=['самолет', 'автомобиль', 'птица', 'кот', 'олень', 'собака', 'лягушка', 'лошадь', 'корабль', 'грузовик']

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

n = 40
plt.imshow(x_train[n])
plt.show()
print("Номер класса:", y_train[n])
print("Тип объекта:", classes[y_train[n][0]])

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

y_train = to_categorical(y_train, nb_classes)
y_test = to_categorical(y_test, nb_classes)


def build_model(hp):
  model = Sequential()
  model.add(Conv2D(filters = hp.Int('conv1_filters', min_value=32, max_value=64, step=16), kernel_size=(3, 3), padding='same', input_shape=(32, 32, 3), activation='relu'))
  model.add(Conv2D(filters = hp.Int('conv1_filters', min_value=32, max_value=64, step=16), kernel_size=(3, 3), activation='relu', padding='same'))
  model.add(MaxPooling2D(pool_size=(2, 2)))
  model.add(Dropout(0.25))

  model.add(Conv2D(64, (3, 3), padding='same', activation='relu'))
  model.add(Conv2D(64, (3, 3), activation='relu'))
  model.add(MaxPooling2D(pool_size=(2, 2)))
  model.add(Dropout(0.25))
  model.add(Flatten())
  model.add(Dense(hp.Int('dense_units', min_value=32, max_value=64, step=16), activation='relu'))
  model.add(Dropout(0.5))
  model.add(Dense(nb_classes, activation='softmax'))

  model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

  return model


tuner = RandomSearch(
    build_model,
    objective='val_accuracy',
    max_trials=5,
    executions_per_trial=2,
    directory='test_dir',
    project_name='pr4'
)

tuner.search(x_train, y_train, epochs=10, validation_data=(x_test, y_test))

top_5_models = tuner.get_best_models(num_models=5)

max_acc = -1
max_acc_idx = -1

for i in range(0, len(top_5_models)):
  _, test_acc = top_5_models[i].evaluate(x_test, y_test)
  print(f"Test accuracy: {test_acc}")
  if test_acc > max_acc:
    max_acc = test_acc
    max_acc_idx = i

# Выбор самой точной из моделей
best_model = top_5_models[max_acc_idx]
print(f"Selected model architecture:")
best_model.summary()
