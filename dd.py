import numpy as np
import matplotlib.pyplot as plt

def parabola(x):
	return x**2

def derivative(x):
  return x*2

x = np.linspace(-10, 10, 100)
y = parabola(x)

solutions = []
evaluations = []

# learning_rate = 0.1
# learning_rate = 0.9
learning_rate = 1.2

# Q = 9
Q = -8

for i in range(20):
  solutions.append(Q)
  evaluations.append(parabola(Q))

  print('%d: f(%.5f) = %.5f, derivative = %.5f' % (i, Q, parabola(Q), derivative(Q)))
  Q = Q - learning_rate * derivative(Q)


plt.plot(x, y)
plt.scatter(solutions, evaluations, color='red')
